# jlink-gdb.el

Interfacing a JLink GDB server and the Emacs (gud-)GDB debugger.

Usage:

``` emacs-lisp
;; Example 1 -- Basic usage
(setq elf-file "my_program.elf")
(setq segger-id "123456789")
(setq device-type "nRF51422_xxAC")
(jlink-gdb-start-gdb elf-file segger-id device-type)

;; Example 2 -- Additional load files
(jlink-gdb-start-gdb
 "main_executable.elf"
 "<SEGGER ID>"
 "<Device name>"
 '(("second_symbol_file.elf" . "0x1000")
   ("third_symbil_file.elf" . "0x24000")))

```
