;;; jlink-gdb.el -- Interfacing JLink GDB server and GDB
;;
;; Copyright (C) 2017 Thomas Stenersen
;;
;; Author: Thomas Stenersen <stenersen.thomas@gmail.com>
(require 's)

(defgroup jlink-gdb nil
  "JLink GDB customization group")

(defcustom jlink-gdb-server-executable
  (cond
   ((eq system-type 'gnu/linux)
    "/usr/bin/JLinkGDBServer")
   ((eq system-type 'windows-nt)
    (concat (car (last (directory-files "c:/Program Files (x86)/SEGGER/" t "JLink" nil))) "/JLinkGDBServerCL.exe")))
  "Path to JLink GDB server executable"
  :type 'string
  :group 'jlink-gdb)

(if (not jlink-gdb-server-executable)
    (warn "Could not find the JLink executable"))

(defcustom jlink-gdb-gdb-executable
  "arm-none-eabi-gdb"
  "GDB executable"
  :type 'string
  :group 'jlink-gdb)

(defcustom jlink-gdb-process-name-prefix
  "jlink-gdb-server"
  "Common name for all JLink GDB Server proecesses."
  :type 'string
  :group 'jlink-gdb)

(defun jlink-gdb-server-process-name (segger-id)
  ;; TODO: To run multiple servers at once, use segger-id to differentiate?
  (concat "*" jlink-gdb-process-name-prefix "*"))

(defun jlink-gdb-server-buffer-name (segger-id)
  (jlink-gdb-server-process-name segger-id))

(defun jlink-gdb--process-name-to-segger-id (process-name)
  (second (s-match (jlink-gdb-server-process-name "\\([0-9]+\\)") process-name)))

(defun jlink-gdb-gdb-sentinel (process event)
  "GDB process sentinel.

Any event to from the process sould kill the JLink GDB server."
  (setq segger-id nil)
  (if (process-live-p (get-process (jlink-gdb-server-process-name segger-id)))
      (kill-process (jlink-gdb-server-process-name segger-id)))

  ;; We sleep for 500ms to let the process completely die before killing the buffer
  (sleep-for 0 500)
  (kill-buffer (jlink-gdb-server-buffer-name segger-id))
  (if (yes-or-no-p "Kill GDB buffer?")
      (kill-buffer)))


(defun jlink-gdb-start-jlink-server (elf-file segger-id &optional device-type)
  (if (not device-type)
      (setq device-type "nRF52832_xxAA"))
  (start-process (jlink-gdb-server-process-name segger-id)
                 (jlink-gdb-server-buffer-name segger-id)
                 jlink-gdb-server-executable
                 "-device" device-type
                 "-if" "SWD"
                 "-speed" "4000"
                 "-noir"
                 "-endian" "little"
                 "-select" (concat "USB=" segger-id)))

(defun jlink-gdb-start-gdb (elf-file segger-id &optional device-type symbols)
  "Start a JLink GDB session for device with SEGGER-ID and ELF-FILE.
DEVICE-TYPE defaults to nRF52832_xxAA."
  (jlink-gdb-start-jlink-server elf-file segger-id device-type)
  (setq args (s-join " " (list jlink-gdb-gdb-executable "-i=mi" elf-file "-ex \"target remote localhost:2331\"")))
  (when symbols
    (setq args (concat args " "
                       (mapconcat (lambda (c) (format "-ex \"add-symbol-file %s %s\"" (car c) (cdr c))) symbols " "))))
  (gdb args)
  (set-process-sentinel (get-process (concat "gud-" (file-name-base elf-file) ".elf")) 'jlink-gdb-gdb-sentinel))

(provide 'jlink-gdb)
